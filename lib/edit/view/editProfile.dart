import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:interview/extra/constant.dart';
import 'package:interview/profile/controller/profileController.dart';

class EditProfile extends StatelessWidget {
  final ProfileController _controller = Get.find();
  @override
  Widget build(BuildContext context) {
    _controller.hasChanged.value = false;
    return WillPopScope(
      onWillPop: _controller.onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Edit Profile",
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          iconTheme: IconTheme.of(context).copyWith(color: Colors.white),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Center(
              child: Container(
                //padding: EdgeInsets.all(10),
                width: Get.width * 0.85,
                alignment: Alignment.center,
                child: Form(
                  key: _controller.formKey,
                  child: Column(
                    children: [
                      Container(
                        //color: AppColor.background.white,
                        margin: EdgeInsets.only(top: 10.0),
                        height: Get.height * 0.2,
                        alignment: Alignment.center,
                        child: Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: primaryColor, width: 2),
                          ),
                          child: InkWell(
                            onTap: () {
                              _controller.capture();
                            },
                            child: Obx(
                              () => _controller.profile.value.picPath
                                      .startsWith('http')
                                  ? CircleAvatar(
                                      minRadius: 65,
                                      maxRadius: 65,
                                      backgroundImage: NetworkImage(
                                          _controller.profile.value.picPath),
                                    )
                                  : CircleAvatar(
                                      minRadius: 65,
                                      maxRadius: 65,
                                      backgroundImage: FileImage(File(
                                          _controller.profile.value.picPath)),
                                    ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: TextFormField(
                          controller: _controller.nameCtrl,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Enter your name',
                            labelStyle: TextStyle(fontSize: 20),
                          ),
                          style: TextStyle(fontSize: 20),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Enter name';
                            }
                            return null;
                          },
                          onChanged: (text) {
                            _controller.hasChanged.value = true;
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: TextFormField(
                          controller: _controller.emailCtrl,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'E-mail',
                            labelStyle: TextStyle(fontSize: 20),
                          ),
                          style: TextStyle(fontSize: 20),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Enter email';
                            }
                            return null;
                          },
                          onChanged: (text) {
                            _controller.hasChanged.value = true;
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: TextFormField(
                          controller: _controller.skillsCtrl,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Skills',
                            labelStyle: TextStyle(fontSize: 20),
                          ),
                          style: TextStyle(fontSize: 20),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Enter Skills';
                            }
                            return null;
                          },
                          onChanged: (text) {
                            _controller.hasChanged.value = true;
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: TextFormField(
                          controller: _controller.expCtrl,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: 'Work experience',
                            labelStyle: TextStyle(fontSize: 20),
                          ),
                          style: TextStyle(fontSize: 20),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Enter experience';
                            }
                            return null;
                          },
                          onChanged: (text) {
                            _controller.hasChanged.value = true;
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: Get.height * 0.2),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30.0),
                          color: primaryColor,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12.withOpacity(0.2),
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset: Offset(0, 2),
                            ),
                          ],
                        ),
                        child: TextButton(
                          onPressed: () {
                            if (_controller.formKey.currentState!.validate()) {
                              if (_controller.hasChanged.value) {
                                Get.back();
                                _controller.saveData();
                                _controller.hasChanged.value = false;
                              }
                            }
                          },
                          child: Container(
                            alignment: Alignment.center,
                            // margin: EdgeInsets.only(left: 30),
                            child: Text(
                              "Save",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
