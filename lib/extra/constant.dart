import 'package:flutter/material.dart';

const ColorSwatch primaryColor = Colors.lightGreen;
const ColorSwatch greyColor = Colors.grey;
const ColorSwatch blueColor = Colors.blueAccent;
const Color boxDecorationColour = Color(0xffdfecfa);
const Color mLightGrey = Color(0xffbbbcbd);
const Color black = Colors.black;
