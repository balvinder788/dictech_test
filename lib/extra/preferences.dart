import 'package:get_storage/get_storage.dart';
import 'package:interview/login/model/loginModel.dart';
import 'package:interview/profile/model/profile.dart';

class Preferences {
  final box = GetStorage();

  LoginModel getLoginDetails() {
    var model = LoginModel();
    model.uName = 'test';
    model.password = '12345';
    return model;
  }

  bool isLoggedIn() {
    var result = box.read('isLogin');
    return result == null ? false : result;
  }

  saveLogin(bool value) {
    box.write('isLogin', value);
  }

  Profile getProfile() {
    var model = Profile();
    var name = box.read('name');
    model.name = name != null ? name : 'Anil Kumar';
    var email = box.read('email');
    model.email = email != null ? email : 'anil@gmail.com';
    var skills = box.read('skills');
    model.skills =
        skills != null ? skills : 'Android, Flutter, Java, Kotlin, Dart';
    var exp = box.read('exp');
    model.experience = exp != null ? exp : '5 Years';

    var path = box.read('path');
    model.picPath = path != null
        ? path
        : 'https://med.gov.bz/wp-content/uploads/2020/08/dummy-profile-pic.jpg';
    return model;
  }

  void saveProfile(Profile profile) {
    box.write('name', profile.name);
    box.write('email', profile.email);
    box.write('skills', profile.skills);
    box.write('exp', profile.experience);
    box.write('path', profile.picPath);
  }

  void clearData() {
    box.remove('name');
    box.remove('email');
    box.remove('skills');
    box.remove('exp');
    box.remove('path');
  }
}
