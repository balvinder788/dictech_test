import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:interview/extra/preferences.dart';
import 'package:interview/profile/view/profilePage.dart';

class LoginController extends GetxController {
  var userController = TextEditingController();
  var passController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  void loginLogic() {
    var user = userController.text.toString();
    var pass = passController.text.toString();

    var prefs = Preferences();
    var model = prefs.getLoginDetails();
    if (user != model.uName) {
      Get.snackbar("", 'Username not matched');
      return;
    }

    if (pass != model.password) {
      Get.snackbar("", 'Password not matched');
      return;
    }
    prefs.saveLogin(true);
    Get.off(ProfilePage());
  }

  void getLogin() {
    var prefs = Preferences();
    var login = prefs.isLoggedIn();
    if (login) {
      Future.delayed(Duration(milliseconds: 100), () {
        Get.off(ProfilePage());
      });
    }
  }
}
