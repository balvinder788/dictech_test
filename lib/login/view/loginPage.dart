import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:interview/extra/constant.dart';
import 'package:interview/login/controller/loginController.dart';

class LoginPage extends StatelessWidget {
  final _controller = Get.put(LoginController());
  /* void setValues() async {
    SharedPreferences shared = await SharedPreferences.getInstance();

    /// set values
    shared.setString('user_name', 'text');
    shared.setString('user_email', 'text123@gmail.com');
  }*/

  @override
  Widget build(BuildContext context) {
    _controller.getLogin();
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Container(
              width: Get.width * 0.85,
              child: Form(
                key: _controller.formKey,
                child: Column(
                  children: [
                    Container(
                      height: Get.height * 0.35,
                      alignment: Alignment.center,
                      child: Text(
                        " LOGIN ",
                        style: TextStyle(
                          color: primaryColor,
                          fontSize: 40,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ),
                    Container(
                      // margin: EdgeInsets.only(
                      //   left: 40,
                      // ),

                      child: TextFormField(
                        controller: _controller.userController,
                        decoration: InputDecoration(
                          //border: InputBorder.none,
                          label: Text('User Name'),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter User Name';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      // margin: EdgeInsets.only(
                      //   left: 40,
                      // ),
                      child: TextFormField(
                        controller: _controller.passController,
                        decoration: InputDecoration(
                          //border: InputBorder.none,
                          label: Text('Password'),
                        ),
                        obscureText: true,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter password';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomRight,
                      margin: EdgeInsets.only(top: 10),
                      child: InkWell(
                        onTap: () {},
                        child: Text(
                          "Forgot Password?",
                          style: TextStyle(color: primaryColor, fontSize: 15),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: Get.height * 0.2),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.0),
                        color: primaryColor,
                        // color: Colors.black,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12.withOpacity(0.2),
                            spreadRadius: 3,
                            blurRadius: 3,
                            offset: Offset(0, 2),
                          )
                        ],
                      ),
                      child: TextButton(
                        onPressed: () async {
                          if (_controller.formKey.currentState!.validate()) {
                            _controller.loginLogic();
                          }
                        },
                        child: Container(
                          alignment: Alignment.center,
                          // margin: EdgeInsets.only(left: 30),
                          child: Text(
                            "Login",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
