import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:interview/extra/preferences.dart';
import 'package:interview/login/view/loginPage.dart';
import 'package:interview/profile/model/profile.dart';

class ProfileController extends GetxController {
  var profile = Profile().obs;
  var prefs = Preferences();
  var nameCtrl = TextEditingController();
  var emailCtrl = TextEditingController();
  var skillsCtrl = TextEditingController();
  var expCtrl = TextEditingController();
  var profilePic = ''.obs;
  final formKey = GlobalKey<FormState>();
  var hasChanged = false.obs;

  void getProfile() {
    profile.value = prefs.getProfile();
    nameCtrl.text = profile.value.name;
    emailCtrl.text = profile.value.email;
    skillsCtrl.text = profile.value.skills;
    expCtrl.text = profile.value.experience;
  }

  void saveData() {
    var model = Profile();
    model.picPath =
        profilePic.value.isEmpty ? profile.value.picPath : profilePic.value;
    model.name = nameCtrl.text;
    model.email = emailCtrl.text;
    model.skills = skillsCtrl.text;
    model.experience = expCtrl.text;

    prefs.saveProfile(model);
  }

  Future<void> capture() async {
    final ImagePicker _picker = ImagePicker();
    var photo = await _picker.pickImage(source: ImageSource.camera);
    profilePic.value = photo!.path;
    profile.value.picPath = photo.path;
    profile.refresh();
    hasChanged.value = true;
  }

  Future<bool> onBackPressed() async {
    // Your back press code here...
    if (hasChanged.value) {
      Get.dialog(Material(
        color: Color(0x80000000),
        child: Center(
          child: Container(
            color: Colors.white,
            height: Get.height * 0.25,
            width: Get.width * 0.85,
            padding: EdgeInsets.all(24),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Do you want to exit without saving data?',
                  style: TextStyle(fontSize: 18),
                ),
                SizedBox(
                  height: 12,
                ),
                Row(
                  children: [
                    TextButton(
                      onPressed: () {
                        Get.back(closeOverlays: true);
                      },
                      child: Text('Exit Anyway'),
                    ),
                    TextButton(
                      onPressed: () {
                        Get.back();
                      },
                      child: Text('Cancel'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ));
    }
    return true;
  }

  void logout() {
    prefs.saveLogin(false);
    prefs.clearData();
    Get.offAll(LoginPage());
  }
}
