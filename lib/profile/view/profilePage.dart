import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:interview/edit/view/editProfile.dart';
import 'package:interview/extra/constant.dart';
import 'package:interview/profile/controller/profileController.dart';

class ProfilePage extends StatelessWidget {
  final _controller = Get.put(ProfileController());
  @override
  Widget build(BuildContext context) {
    _controller.getProfile();
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Profile",
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {
              Get.to(EditProfile())!.then((value) {
                _controller.getProfile();
              });
            },
            icon: Icon(
              Icons.edit,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(10),
          width: Get.width * 90,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  //color: AppColor.background.white,
                  margin: EdgeInsets.only(top: 10.0),
                  height: Get.height * 0.2,
                  alignment: Alignment.center,
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: primaryColor, width: 2),
                    ),
                    child: InkWell(
                      onTap: () {},
                      child: Obx(
                        () => _controller.profile.value.picPath
                                .startsWith('http')
                            ? CircleAvatar(
                                minRadius: 65,
                                maxRadius: 65,
                                backgroundImage: NetworkImage(
                                    _controller.profile.value.picPath),
                              )
                            : CircleAvatar(
                                minRadius: 65,
                                maxRadius: 65,
                                backgroundImage: FileImage(
                                    File(_controller.profile.value.picPath)),
                              ),
                      ),
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Name",
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  width: Get.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: greyColor, width: 2),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(left: 10),
                    padding: EdgeInsets.all(8),
                    child: Obx(
                      () => Text(
                        "${_controller.profile.value.name}",
                        style: TextStyle(color: black, fontSize: 20),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 18),
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Email",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  width: Get.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: greyColor, width: 2),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(left: 10),
                    padding: EdgeInsets.all(8),
                    child: Obx(
                      () => Text(
                        "${_controller.profile.value.email}",
                        style: TextStyle(color: black, fontSize: 20),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 18),
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Skills",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  width: Get.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: greyColor, width: 2),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(left: 10),
                    padding: EdgeInsets.all(8),
                    child: Text(
                      "${_controller.profile.value.skills}",
                      style: TextStyle(
                        color: black,
                        fontSize: 20,
                      ),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 18),
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Work experience",
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  width: Get.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: greyColor, width: 2),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(left: 10),
                    padding: EdgeInsets.all(8),
                    child: Text(
                      "${_controller.profile.value.experience}",
                      style: TextStyle(color: black, fontSize: 20),
                    ),
                  ),
                ),
                SizedBox(
                  height: 24,
                ),
                Container(
                  child: ElevatedButton(
                    onPressed: () {
                      _controller.logout();
                    },
                    child: Text(
                      'Logout',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
